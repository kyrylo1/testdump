'use strict';

const express = require('express');
const app = express();

console.log(process.memoryUsage());

const RecorderUser = require('./RecorderUser');



(() => {
    const router = express.Router();

    router.get('/', (req, res) => {
        res.send('The app is listening on internal port 80');
    });

    router.get('/health', (req, res) => {
        res.json({status: 'UP'});
    });

    /*
        heapTotal and heapUsed refer to V8's memory usage.
        external refers to the memory usage of C++ objects bound to JavaScript objects managed by V8.
        rss, Resident Set Size, is the amount of space occupied in the main memory device (that is a subset of the total allocated memory) for the process, including all C++ and JavaScript objects and code.
        arrayBuffers refers to memory allocated for ArrayBuffers and SharedArrayBuffers, including all Node.js Buffers. This is also included in the external value. When Node.js is used as an embedded library, this value may be 0 because allocations for ArrayBuffers may not be tracked in that case.
    */
    router.get('/memory', (req, res) => {
        const memoryUsage = process.memoryUsage();
        console.log(memoryUsage);

        Object.keys(memoryUsage).forEach(function (key) {
            memoryUsage[key] = Math.round(memoryUsage[key] / 1000000.0 * 10) / 10;
        });
        res.json(memoryUsage);
    });

    router.get('/start', (req, res) => {
        const recorderUser = new RecorderUser();

        recorderUser.encode(_ => {
            const memory = process.memoryUsage();
            console.log(memory);

            Object.keys(memory).forEach(function (key) {
                memory[key] = Math.round(memory[key] / 1000000.0 * 10) / 10;
            });
            res.json(memory);
        });

        recorderUser.recordChunks(100000);
        recorderUser.stop();
    });

    app.use("/", router);

    app.listen(80, () => {
        console.log('\x1b[33m%s\x1b[0m', '*** The app is listening on internal port 80 ***');
    });
})();
