'use strict';

const Recorder = require('./Recorder');
const path = require('path');
const downloadFolder = path.join(require('os').homedir(), 'Downloads');
const fs = require('fs');

module.exports = class RecorderUser {
    constructor() {
        this.recorder = new Recorder('output_orig');;
        this.frameData = fs.readFileSync('./Samples/frame.out');
        this.sampleData = fs.readFileSync('./Samples/sample.out');
    }

    recordChunks(count) {
        for (let i = 0; i <= count; i++) {
            this.recorder.recordVideoChunks(Buffer.from(this.frameData));
            this.recorder.recordAudioChunks(Buffer.from(this.sampleData));
        }
    }

    encode(callback) {
        this.recorder.start(callback);
    }

    stop() {
        setTimeout(() => this.recorder.stop(), 4000);
    }
}