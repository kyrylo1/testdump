'use strict';

const Recorder = require('./Recorder');
const path = require('path');
const downloadFolder = path.join(require('os').homedir(), 'Downloads');
const fs = require('fs');


const frameCount = 100;
const recorder = new Recorder('output_orig');

const frameData = fs.readFileSync(downloadFolder + '/Samples/frame.out');
const sampleData = fs.readFileSync(downloadFolder + '/Samples/sample.out');
for (let i = 0; i <= frameCount; i++) {
    recorder.recordVideoChunks(frameData);
    recorder.recordAudioChunks(sampleData);
}

recorder.start(_ => {})
//sleep(10)
setTimeout(() => recorder.stop(), 4000)