const ffmpeg = require('fluent-ffmpeg')
const {PassThrough} = require('stream')
const path = require('path')
const downloadFolder = path.join(require('os').homedir(), 'Downloads')

const net = require('net')
const fs = require('fs')

let counter = 0

class UnixStream {
    constructor(stream, onSocket) {
        const path = `./${++counter}.sock`
        this.url = 'unix:' + path

        if (fs.existsSync(path)) {
            fs.unlinkSync(path)
        }

        const server = net.createServer(onSocket)

        stream.on('finish', () => {
            server.close()
        })

        stream.on('error', err => {
            console.log('net stream error', err)
        })


        server.on('error', err => {
            console.log('net server error', err)
        })
        server.on('close', () => {
            console.log('net server was closed');
        })
        server.listen(path, () => {
            console.log(`[DEBUG] Server.listen ${path}`);
        })
    }

    static create(stream) {
        return Object.freeze(new UnixStream(stream, socket => {
            stream.pipe(socket);
        }))
    }
}

module.exports = class Recorder {
    constructor(outputFile, ext = 'mp4') {
        this.ext = ext
        this.stream = {
            recordPath: path.join(downloadFolder, outputFile),
            videoStream: new PassThrough({highWaterMark: 1 << 16}),
            audioStream: new PassThrough({highWaterMark: 1 << 16})
        }

        if (!fs.existsSync(downloadFolder)) {
            fs.mkdirSync(downloadFolder)
        }
    }

    recordVideoChunks(frameData) {
        this.stream.videoStream.push(frameData)
    }

    recordAudioChunks(sampleData) {
        this.stream.audioStream.push(sampleData)
    }

    start(callback) {
        const videoUnixStream = UnixStream.create(this.stream.videoStream)
        const audioUnixStream = UnixStream.create(this.stream.audioStream)

        const outputOptions = [
            '-c:v libx264', //'-c:v libvpx-vp9',
            '-c:a aac',
            // '-shortest', // REVISIT: enabling this flag, causes the ffmpeg process to not trigger the "end" event forvideos longer than 10s
            '-preset veryfast'
        ]

        if (fs.existsSync(videoUnixStream.url.substring(5))) {
            const stats = fs.statSync(videoUnixStream.url.substring(5));
            console.log(`[DEBUG] VideoUnixStream stats ${JSON.stringify(stats)}`);
        }
        if (fs.existsSync(audioUnixStream.url.substring(5))) {
            const stats = fs.statSync(audioUnixStream.url.substring(5));
            console.log(`[DEBUG] AudioUnixStream stats ${JSON.stringify(stats)}`);
        }

        ffmpeg()
            .addInput(videoUnixStream.url)
            .addInputOptions([
                '-f:v', 'rawvideo',
                '-pix_fmt', 'yuv420p',
                '-s', '640x480'
            ])
            .addInput(audioUnixStream.url)
            .addInputOptions([
                '-f:a s16le',
                '-ar 48k',
                '-ac 1'
            ])
            .outputFormat(this.ext)
            .addOutputOptions(outputOptions)
            .output(`${this.stream.recordPath}.${this.ext}`)
            .on('start', (commandLine) => {
                console.log('Spawned ffmpeg with command: ' + commandLine)
            })
            .on('end', () => {
                console.log(`Video encoding success, file: ${this.stream.recordPath}.${this.ext}`);
                callback();
            })
            .on('error', (err) => {
                console.error(`[${new Date().toUTCString()}] [ERROR] Video encoding error: ${err.message}`)
            })
            .on('stderr', stderrLine => {
                console.log('Encoding:', stderrLine)
            })
            .run()
    }

    stop() {
        this.stream.videoStream.end();
        this.stream.audioStream.end();

        //this.stream.videoStream = null;
        //this.stream.audioStream = null;
    }
}
